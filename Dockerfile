FROM cloudron/base:3.2.0@sha256:ba1d566164a67c266782545ea9809dc611c4152e27686fd14060332dd88263ea

SHELL ["/bin/bash", "-o", "pipefail", "-c"]

RUN mkdir -p /app/pkg

ENV \
    DOCKERIZE_VERSION=v0.11.0 \
    MEET_VERSION=2.3.1-0+359.1

RUN curl -sfL https://github.com/powerman/dockerize/releases/download/"$DOCKERIZE_VERSION"/dockerize-"$(uname -s)"-"$(uname -m)" | install /dev/stdin /bin/dockerize && \
    dockerize --version

ARG SERIAL_KEY=xxx
# hadolint ignore=DL3008
RUN \
    echo "deb https://serial:${SERIAL_KEY}@download.kopano.io/supported/meet:/master/Ubuntu_20.04/ /" >> /etc/apt/sources.list.d/kopano-meet.list && \
    echo "deb https://serial:${SERIAL_KEY}@download.kopano.io/supported/core:/final/Ubuntu_20.04/ /" >> /etc/apt/sources.list.d/kopano-meet.list && \
    curl "https://serial:${SERIAL_KEY}@download.kopano.io/supported/core:/final/Ubuntu_20.04/Release.key" | apt-key add - && \
    apt-get update -y && \
    apt-get install -y python3-ujson && \
    pip3 install --no-cache-dir yq==2.7.2 && \
    apt search kopano-meet-packages && \
    apt-get install -y --no-install-recommends kopano-meet-packages=$MEET_VERSION kopano-grapi python3-grapi.backend.ldap && \
    rm /etc/apt/sources.list.d/kopano-meet.list && \
    rm -rf /var/cache/apt /var/lib/apt/lists/*

# add supervisor configs
RUN sed -e 's,^logfile=.*$,logfile=/run/supervisord.log,' -i /etc/supervisor/supervisord.conf
COPY supervisor/* /etc/supervisor/conf.d/

RUN rm -f /etc/kopano/konnectd-signing-private-key.pem && ln -s /app/data/konnectd-signing-private-key.pem /etc/kopano/konnectd-signing-private-key.pem && \
    rm -f /etc/kopano/konnectd-encryption-secret.key && ln -s /app/data/konnectd-encryption-secret.key /etc/kopano/konnectd-encryption-secret.key && \
    mv /etc/kopano/kwmserverd.cfg{,-orig} && ln -s /app/data/kwmserverd.cfg /etc/kopano/kwmserverd.cfg && \
    mv /etc/kopano/konnectd.cfg{,-orig} && ln -s /app/data/konnectd.cfg /etc/kopano/konnectd.cfg && \
    mkdir -p /etc/kopano/kweb/overrides.d/config/kopano && \
    ln -s /run/meet.json /etc/kopano/kweb/overrides.d/config/kopano/meet.json

COPY konnectd-identifier-registration.yaml konnectd-identifier-scopes.yaml /etc/kopano/
COPY meet.json /app/pkg/config.json.in
COPY konnect-wrapper.sh kwmserver-wrapper.sh /usr/local/bin/
COPY start.sh /app/pkg/

WORKDIR /app/data/

CMD [ "/app/pkg/start.sh" ]
