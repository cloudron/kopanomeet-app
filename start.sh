#!/bin/bash

set -eu

if [[ -e /app/data/guests-disabled ]]; then
	GUESTMODE=no
fi

if [[ ! -f /app/data/konnectd-signing-private-key.pem ]]; then
	echo "=> Creating new RSA private key at /app/data/konnectd-signing-private-key.pem ..."
    RANDFILE=/tmp/.rnd openssl genpkey -algorithm RSA -out "/app/data/konnectd-signing-private-key.pem" -pkeyopt rsa_keygen_bits:4096 -pkeyopt rsa_keygen_pubexp:65537
fi

if [[ ! -f /app/data/konnectd-encryption-secret.key ]]; then
    echo "=> Creating new secret key at /app/data/konnectd-encryption-secret.key ..."
	RANDFILE=/tmp/.rnd openssl rand -out "/app/data/konnectd-encryption-secret.key" 32
fi

if [[ ! -f /app/data/kwmserverd.cfg ]]; then
    echo "=> Creating new kwmserver configuration at /app/data/kwmserver.cfg ..."
    cp /etc/kopano/kwmserverd.cfg-orig /app/data/kwmserverd.cfg
fi

if [[ ! -f /app/data/konnectd.cfg ]]; then
    echo "=> Creating new kwmserver configuration at /app/data/konnectd.cfg ..."
    cp /etc/kopano/konnectd.cfg-orig /app/data/konnectd.cfg
fi

echo "=> Set STUN/TURN server"
sed -e "s/.*turn_uris =.*/turn_uris = turn:${CLOUDRON_TURN_SERVER}:${CLOUDRON_TURN_PORT}/" -i /app/data/kwmserverd.cfg
sed -e "s,.*turn_server_shared_secret =.*,turn_server_shared_secret = /run/turn.secret," -i /app/data/kwmserverd.cfg
echo "${CLOUDRON_TURN_SECRET}" > /run/turn.secret

cp /app/pkg/config.json.in /run/meet.json
CONFIG_JSON=/tmp/konnectd-identifier-registration.yaml
identifier_registration_conf=/app/data/konnectd-identifier-registration.yaml
if [ "${GUESTMODE:-}" = "no" ]; then
	echo "Guest access is not enabled. Delete '/app/data/guests-disabled' to enable guest access."
    # disable guest settings
    sed -e "s,.*allow_client_guests =.*,allow_client_guests = no," -i /app/data/konnectd.cfg
    sed -e "s,.*enable_guest_api =.*,enable_guest_api = no," -i /app/data/kwmserverd.cfg
else
	echo "Enabling guest access. Create '/app/data/guests-disabled' to disable guest access."
    touch /app/data/konnectd-identifier-registration.yaml
    sed -e "s,.*allow_client_guests =.*,allow_client_guests = yes," -i /app/data/konnectd.cfg
    sed -e "s,.*enable_guest_api =.*,enable_guest_api = yes," -i /app/data/kwmserverd.cfg
    sed -e "s,.*public_guest_access_regexp =.*,public_guest_access_regexp = ^group/public/.*," -i /app/data/kwmserverd.cfg

	# Create working copy by merging packaged example in /etc/kopano with passed registration conf
	yq -y -s '.[0] + .[1]' /etc/kopano/konnectd-identifier-registration.yaml "${identifier_registration_conf:?}" | sponge "$CONFIG_JSON"

	# only modify identifier registration if it does not already contain the right settings
	if ! yq .clients[].id /app/data/konnectd-identifier-registration.yaml | grep -q "kpop-https://${CLOUDRON_APP_DOMAIN%/*}/meet/"; then

		ecparam=${ecparam:-/app/data/ecparam.pem}
		eckey=${eckey:-/app/data/meet-kwmserver.pem}

		# Key generation for Meet guest mode
		if [ ! -s "$ecparam" ]; then
			echo "Creating ec param key for Meet guest mode ..."
			openssl ecparam -name prime256v1 -genkey -noout -out "$ecparam" >/dev/null 2>&1
		fi

		if [ ! -s "$eckey" ]; then
			echo "Creating ec private key for Meet guest mode..."
			openssl ec -in "$ecparam" -out "$eckey" >/dev/null 2>&1
		fi

		echo "Entrypoint: Patching identifier registration for use of the Meet guest mode"
		/usr/sbin/kopano-konnectd utils jwk-from-pem --use sig "$eckey" > /tmp/jwk-meet.json
		yq -y ".clients += [{\"id\": \"kpop-https://${CLOUDRON_APP_DOMAIN%/*}/meet/\", \"name\": \"Kopano Meet\", \"application_type\": \"web\", \"trusted\": true, \"redirect_uris\": [\"https://${CLOUDRON_APP_DOMAIN%/*}/meet/\"], \"trusted_scopes\": [\"konnect/guestok\", \"kopano/kwm\"], \"jwks\": {\"keys\": [{\"kty\": $(jq .kty /tmp/jwk-meet.json), \"use\": $(jq .use /tmp/jwk-meet.json), \"crv\": $(jq .crv /tmp/jwk-meet.json), \"d\": $(jq .d /tmp/jwk-meet.json), \"kid\": $(jq .kid /tmp/jwk-meet.json), \"x\": $(jq .x /tmp/jwk-meet.json), \"y\": $(jq .y /tmp/jwk-meet.json)}]},\"request_object_signing_alg\": \"ES256\"}]" $CONFIG_JSON >> /tmp/guest-mode.yml
		yq -y -s '.[0] + .[1]' $CONFIG_JSON /tmp/guest-mode.yml | sponge "$identifier_registration_conf"
	else
		echo "Entrypoint: Skipping guest mode configuration, as it is already configured."
	fi
    jq '.guests.enabled=true' /run/meet.json | sponge /run/meet.json
fi

echo "=> Ensure permissions"
chown -R cloudron:cloudron /run /app/data

echo "=> Start supervisor"
exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i kopano
