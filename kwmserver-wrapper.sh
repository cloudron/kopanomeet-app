#!/bin/bash

dockerize \
	-wait "$oidc_issuer_identifier"/.well-known/openid-configuration \
	-timeout 360s

sed s/\ *=\ */=/g /app/data/kwmserverd.cfg > /tmp/kwmserverd-env
# shellcheck disable=SC2046
export $(grep -v '^#' /tmp/kwmserverd-env | xargs -d '\n')

exec /usr/sbin/kopano-kwmserverd serve