# Kopano Meet

Kopano Meet is a secure, open-source and easy-to-use solution for video conferencing. It is peer-to-peer and end-to-end encrypted which means that a conversation always takes place directly between the people in the call, with no other parties in between. Kopano Meet was developed as a progressive web app so interaction with Meet feels exactly the same, no matter the device that is used.

## Installation

[![Install](https://cloudron.io/img/button32.png)](https://cloudron.io/button.html?app=com.kopano.meet.cloudronapp)

or using the [Cloudron command line tooling](https://cloudron.io/references/cli.html)

```bash
cloudron install --appstore-id com.kopano.meet.cloudronapp
```

## Building

### Cloudron

The app package can be built using the [Cloudron command line tooling](https://cloudron.io/references/cli.html).

```bash
git clone https://git.cloudron.io/cloudron/kopanomeet-app.git
cd kopanomeet-app
cloudron build
cloudron install
```
